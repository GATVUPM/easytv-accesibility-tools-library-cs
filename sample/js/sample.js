
angular.module('easytv',[])
	.controller("myCtrl",function($scope,EasyTVToolsLibrary) {
		//localStorage.removeItem("options");
		$scope.url="http://138.4.47.33:2103/jlv/proyectos/hbbtv/www/contenidos-dash-EasyTV/CSFA/manifest-accessibility.mpd"
		$scope.options=EasyTVToolsLibrary.initDefaultOptions();
		$scope.$watch("options", function(newValue){
			EasyTVToolsLibrary.updateOptions(newValue);
		},true); 
		var player= dashjs.MediaPlayer().create();
		var media = $('<video />', {
				id: 'video',
				controls: false,
			});
		setTimeout(function(){
			$('#player').prepend(media);
			player.initialize(document.querySelector("#video"), $scope.url, false);
			video.addEventListener("timeupdate", updateTime);
			EasyTVToolsLibrary.init($scope.url,"video",player);
		},1);
		$scope.play=function(){
			video.play();
		}
		$scope.pause=function(){
			video.pause();
		}
		$scope.toggleSlow=function(){
			if ($scope.options.playbackRate==1){
				$scope.options.playbackRate=0.5
			}else{
				$scope.options.playbackRate=1
			}
		}
		var updateTime = function() {
			if(video.duration!=null){
				var value = (100 / video.duration) * video.currentTime;
				$("#seek-bar").val(value);
			}
		}
	});