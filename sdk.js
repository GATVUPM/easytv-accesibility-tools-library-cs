angular
	.module("easytv")
	.factory("EasyTVToolsLibrary",["$http","$q","$rootScope",function($http,$q,$rootScope){ 
		var factory ={};
		var facesJson=[];
		var facesReady=false;
		var facesTimeout=null;
		var textJson=[];
		var textReady=false;
		var textDetectionTimeout=null;
		var scenes=[];
		var scenesReady=false;
		var video=null;
		var player=null;
		var zoom=false;
		var textSwiped="";
		var style = document.createElement("style");
		var options={}
		document.head.appendChild(style);

		var readMPD= function(xml) {
			var xmlDoc = xml.responseXML;
			var vx = xmlDoc.getElementsByTagName("face-detection");
			if(vx[0]){
				$http({method: 'GET', url: vx[0].firstChild.nodeValue+"?"+ Date.now()}).then(function successCallback(response) {
					facesJson=response.data;
					facesReady=true;
				},function errorCallback(response) {
					facesReady=false;
				});
			}
			vx = xmlDoc.getElementsByTagName("text-detection");
			if(vx[0]){
					$http({method: 'GET', url: vx[0].firstChild.nodeValue+"?"+ Date.now()}).then(function successCallback(response) {
					textJson=response.data.text_read_in_video;
					textReady=true;

					},function errorCallback(response) {
						textReady=false;
					});
			}
			vx = xmlDoc.getElementsByTagName("scenes-detection");
			if(vx[0]){
				$http({method: 'GET', url:vx[0].firstChild.nodeValue+"?"+ Date.now()}).then(function successCallback(response) {
					scenes=response.data;
					//console.log(scenes);
					//setTimeout(scenesDetection,10); /TODO Scenes Detection logic
				}, function errorCallback(response) {
					scenesReady=false;
				});
			}
		}
		var textDetection= function(){
			if(options.textDetection){
				position=""+Math.round(video.currentTime)*25;
				if(textJson[position]!=null && textSwiped!=textJson[position].text_stored){
					$("#textDetected").show();
					$("#textDetected").html(textJson[position].text_stored);
					textSwiped="";
				}else{
					$("#textDetected").html("");
				}
				setTimeout(textDetection,1000);
			}else{
				$("#textDetected").html("");
			} 
		}
		var faceDetection= function(){
			if(options==null || options.enhancement==null ){
				console.warn("Enhancement options have not been setted")
			}
			if(video){
				if(options.enhancement.magnification.type=="faces" && options.enhancement.enabled){
					position=""+Math.round(video.currentTime);
					if(facesJson[position]!=null){
						zoomFace(facesJson[position].x*(video.clientWidth/1280),facesJson[position].y*(video.clientHeight/720));
					}
					facesTimeout = setTimeout(faceDetection,1000);
				} 
			}
		}
		var zoomFace =function(x,y){

			zoom = true;
			if(x==0 && y==0){
				var z=1;
				var tX = 0;
				var tY = 0;
			}else{
				var z=2;
				var tX = ((video.clientWidth/2)-(+x));
				var tY = ((video.clientHeight/2)-(+y));
			}

			video.style.transform = "matrix("+z+ "," +0+ "," +0+ "," +z+ "," + tX + "," +  tY + ")";
			hideAllSubtitles();
		}
		var videoClick=function(event) {
			if(zoom){
				if(options==null || options.enhancement==null || options.enhancement.magnification == null || options.enhancement.magnification.scale==null || options.enhancement.magnification.scale==0 ){
					console.warn("Enhancement magnifications options have not been setted (scale shall not be 0)")
				}else{
					var tX = ((video.clientWidth/2)-(+event.offsetX));
					var tY = ((video.clientHeight/2)-(+event.offsetY));
					video.style.transform = "matrix("+options.enhancement.magnification.scale+ "," +0+ "," +0+ "," +options.enhancement.magnification.scale+ "," + tX + "," +  tY + ")";
				}
			}
		}

		var videoDblClick=function(event) {
			if(options==null || options.enhancement==null ){
				console.warn("Enhancement options have not been setted")
			}else{
				if(options==null || options.enhancement==null ){
					console.warn("Enhancement options have not been setted")
				}else{
					if(options.enhancement.magnification== null || options.enhancement.magnification==null || options.enhancement.magnification.sclae==0){
						console.warn("Enhancement magnifications options have not been setted (scale shall not be 0)")
					}else{
						if(options.enhancement.magnification.type=="magnification" && options.enhancement.enabled){
							sp={};
							sp.x=event.offsetX-(video.offsetWidth/options.enhancement.magnification.scale)/2;
							sp.y=event.offsetY+(video.offsetHeight/options.enhancement.magnification.scale)/2;
							if(!zoom){
								zoom=true;
								var tX = ((video.clientWidth/2)-(+event.offsetX));
								var tY = ((video.clientHeight/2)-(+event.offsetY));
								video.style.transform = "matrix("+options.enhancement.magnification.scale+ "," +0+ "," +0+ "," +options.enhancement.magnification.scale+ "," + tX + "," +  tY + ")";
								hideAllSubtitles();
							}else{
								video.style.transform='scale(1)';
								changeSubtitlesSize();
								changeSubtitles();
								zoom=false;
							}
						}else if(options.enhancement.magnification.type=="faces" && options.enhancement.enabled && facesReady){
								options.enhancement.enabled=false;
								$rootScope.$apply();
								video.style.transform='scale(1)';
								changeSubtitlesSize();
								changeSubtitles();
								zoom=false;
						}
					}
				}
			}
		}

		var checkEnhancement= function(){
			if(options==null || options.enhancement == null ){
				console.warn("Enhancement options have not been setted")
			}else if(video){
				if(! options.enhancement.enabled ){
					video.style.transform='scale(1)';
					changeSubtitlesSize();
					changeSubtitles();
					zoom=false;
				 }else{
					if(options.enhancement.magnification.type=="faces"){
						if(facesTimeout){
							clearTimeout(facesTimeout);
						}
						facesTimeout=setTimeout(faceDetection,1000);
					}
				}
			}
		}

		var checkTextDetection = function (){
			if(options==null || options.textDetection==null ){
				console.warn("Text Detection options have not been setted")
			}else if(video){
				video.style.transform='scale(1)';
				changeSubtitlesSize();
				changeSubtitles();
				zoom=false;
				if(options.textDetection){
					if(textDetection){
						clearTimeout(textDetection);
					}
					textDetectionTimeout=setTimeout(textDetection,1000);
				}else{
					$("#textDetected").html("");
				}
			}
		}

		var  hideAllSubtitles = function(){
			player.enableText(false);
		}

		var changeSubtitles = function(){
			if(options== null || options.subtitles==null || options.subtitles.language ==null){
				console.warn("Subtitles options have not been setted ")
			}else if(video){
				player.enableText(false);
				for(i=0;i<video.textTracks.length;i++){
					if(video.textTracks[i].language==options.subtitles.language){
						player.enableText(true);
						player.setTextTrack(i);
					}
				}
			}
		}

		var changeCSSRule = function (selector,rule){

			
			if (style.sheet.insertRule) {
				style.sheet.insertRule(selector + rule, style.sheet.cssRules.length);
			} else if (style.sheet.addRule) {
				style.sheet.addRule(selector, rule, -1);
			}
		}

		var changeSubtitlesSize = function(){	
			if(options== null || options.subtitles==null || options.subtitles.size==null){
				console.warn("Subtitles options have not been setted ")
			}else{
				changeCSSRule("::cue", "{font-size: "+options.subtitles.size+"px}");
				changeCSSRule("#textDetected", "{font-size: "+options.subtitles.size+"px}");	  
			}
		}

		var changeSubtitlesBackground = function(){
			if(options== null || options.subtitles==null || options.subtitles.background==null){
				console.warn("Subtitles options have not been setted ")
			}else{
				changeCSSRule("::cue", "{background-color:"+options.subtitles.background+"}");
				changeCSSRule("#textDetected","{background-color:"+options.subtitles.background+"}");
			}
		}

		var changeSubtitlesColor = function(){
			if(options== null || options.subtitles==null || options.subtitles.background==null){
				console.warn("Subtitles options have not been setted ")
			}else{
				changeCSSRule("::cue", "{color:"+options.subtitles.color+"}");
				changeCSSRule("#textDetected", "{color:"+options.subtitles.color+"}");
			}
		}

		var changePlaybackRate =function(){
			if(video){
				video.playbackRate=options.playbackRate
			}
		}

		var difference = function(o1, o2) {
			var k, kDiff,
				diff = {};
			for (k in o1) {
				if (!o1.hasOwnProperty(k)) {
				} else if (typeof o1[k] != 'object' || typeof o2[k] != 'object') {
					if (!(k in o2) || o1[k] !== o2[k]) {
						diff[k] = o2[k];
					}
				} else if (kDiff = difference(o1[k], o2[k])) {
					diff[k] = kDiff;
				}
			}
			for (k in o2) {
				if (o2.hasOwnProperty(k) && !(k in o1)) {
					diff[k] = o2[k];
				}
			}
			for (k in diff) {
				if (diff.hasOwnProperty(k)) {
					return diff;
				}
			}
			return false;
		}

		factory.init=function (url,videoID,scopePlayer){

			var xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					readMPD(this);
					
				}
			};
			xmlhttp.open("GET", url+"?"+ Date.now(), true);
			xmlhttp.send();
			video = document.getElementById(videoID);
			$(video).parent().prepend("<div id=\"textDetected\" >")
			changeCSSRule("#textDetected", "{position:absolute}");
			changeCSSRule("#textDetected", "{z-index:5}");
			player=scopePlayer;
			video.addEventListener('click', videoClick, false);
			video.addEventListener("dblclick", videoDblClick);
			checkEnhancement();
			checkTextDetection();
			setTimeout(changeSubtitles,1000);
			changeSubtitlesSize();
			changeSubtitlesBackground();
			changeSubtitlesColor();
			changePlaybackRate();
		}
		

		factory.initDefaultOptions=function(){
			options= JSON.parse(localStorage.options || null) || { enhancement: { enabled: false, magnification:{type :"magnification",scale:2.5}},textDetection:false,subtitles:{language:"es",size:24,background:"#000000",color:"#FFFF00"},playbackRate:1};
			return JSON.parse(JSON.stringify(options));
		}
		
		factory.updateOptions=function(newValue){
				options=newValue;	
				changeSubtitles();
				changeSubtitlesSize();
				changeSubtitlesBackground();
				changeSubtitlesColor();
				changePlaybackRate();
				checkEnhancement();
				checkTextDetection();
				localStorage.options=JSON.stringify(options);
		}
		
		factory.destroy = function(){
			video.removeEventListener('click', videoClick, false);
			video.removeEventListener("dblclick", videoDblClick);
			facesJson=[];
			facesReady=false;
			textJson=[];
			textReady=false;
			scenes=[];
			scenesReady=false;
			video=null;
			player=null;
			zoom=false;
			clearTimeout(facesTimeout);
			clearTimeout(textDetectionTimeout);
		}
		return factory;

	}])